/**
 * 
 */
package com.snjv.assesment;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Sanjeev Rathaur
 *
 */
public class MancalaTest {
	
	private Mancala mancala = null;

	@Before
	public void setUp() {
		mancala = new Mancala();
	}

	@After
	public void tearDown() {
	}
	
	@Test
	public void testDefaultConfs() {
		assertEquals("Total locations on board are 14.", mancala.board.length, 14);
		assertEquals("Player one takes first turn.", mancala.getTurn(), Player.One);
	}

	/**
	 * Test method for {@link com.snjv.assesment.Mancala#switchTurn()}.
	 */
	@Test
	public final void testSwitchTurn() {
		assertEquals("Player one turn.", mancala.getTurn(), Player.One);
		mancala.switchTurn();
		assertEquals("Player two turn.", mancala.getTurn(), Player.Two);
	}

	
}
