package com.snjv.assesment;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 *  This class is for making mancala game (Commandline base)
 * @author Sanjeev Rathaur
 */
class Mancala {

    private Player turn;
    protected boolean computer;
    public static int[] board;

    Mancala() {
        board = new int[14];
        turn = Player.One;
    }

    public static void main(String[] args) throws InterruptedException {
        Mancala game = new Mancala();
        System.out.println("  ---------------------");
        System.out.println("  ------Mancala Game--------");
        System.out.println("  ---------------------");
        System.out.println();
        boolean playAgain = true;
        Scanner scanner;
        while (playAgain) {
            scanner = new Scanner(System.in);
            System.out.print("Press 1 to play with the computer, 2 to play with a human (1 or 2):");
            if (scanner.nextLine().contains("1")) {
                game.computer = true;
            }
            game.reset();
            game.printBoard();
            
            while (!game.isOver()) {
                boolean again = true;
                while (again) {
                    int position = 0;
                    if (game.computer && game.getTurn() == Player.Two) {
                        System.out.println("The computer is thinking...");
                        Thread.sleep(1800);
                        position = game.choose();
                    } else {
                        System.out.print("Player " + game.getTurn() + ", choose which pile to take (1-6):");
                        position = game.readValue(scanner);
                    }
                    again = game.markBoard(position);
                }
                game.switchTurn();
            }
            System.out.println("The game is over! Press enter to count the pieces.");
            scanner.nextLine();
            Player winner = game.getWinner();
            game.printBoard();
            game.printWin(winner);
            System.out.print("\nPlay Again? (Y/N):");
            if (scanner.nextLine().equalsIgnoreCase("n")) {
                playAgain = false;
            }
        }
    }

    // Get the current turn
    // Returns the Player who has the current turn
    protected Player getTurn() {
        return turn;
    }

    // Chooses a location for the computer to play Returns an integer of the location
    public int choose() {
        Player computer = Player.Two;
        int start = (computer.getSkip() + 1) % board.length;

        List<Integer> validLocations = IntStream.range(start, start + (board.length - 1) / 2).filter(i -> board[i] > 0)
                .boxed().collect(Collectors.toList());

        // If any location lets us go again, return the first we find

        Optional<Integer> kalahLocation = validLocations.stream()
                .filter(location -> (location + board[location]) % board.length == computer.getKalahLoc()).findFirst();
        if (kalahLocation.isPresent()) {
            return kalahLocation.get();
        }

        // If any location lets us capture pieces, return the one that gives us the most
        // pieces
        int maxPieces = 0;
        int pos = validLocations.get(0);
        for (int location : validLocations) {
            int landingPos = (location + board[location]) % board.length;
            int oppositePos = getOpposite(landingPos);
            if (board[landingPos] == 0 && board[oppositePos] > maxPieces && oppositePos != location) {
                maxPieces = board[oppositePos];
                pos = location;
            }
        }

        // found at least one opportunity to capture pieces, let's return it
        if (maxPieces > 0) {
            System.out.println("All your pieces are belong to me!");
            return pos;
        }
        // if No particular location looks advantageous. Just return a random location
        return validLocations.get(new Random().nextInt(validLocations.size()));
    }

    // Switches the turn 
    public void switchTurn() {
       
        turn = turn == Player.One ? Player.Two : Player.One;

    }

    // Prints the winner of the game
    public void printWin(Player winner) {
        if (winner == Player.One) {
            if (computer) {
                System.out.println("Congratulations, you win!");
            } else {
                System.out.println("Player One WINS!");
            }
        } else if (winner == Player.Two) {
            if (computer) {
                System.out.println("The computer beat you!");
                System.out.println("It says: too hard? :)");
            } else {
                System.out.println("Player Two WINS!");
            }
        } else {
            System.out.println("The game is a tie!");
        }
    }

    // Reset game board
    public void reset() {
        
        int STARTING_AMOUNT = 4;
        Arrays.fill(board, STARTING_AMOUNT);
        
        Arrays.stream(Player.values()).forEach(this::resetBoardIndex);

        turn = Player.One;
    }

    void resetBoardIndex(Player player) {
        board[player.getKalahLoc()] = 0;
    }

    // Prints game state
    public void printBoard() {
        if (!computer) {
            System.out.println("    (1) (2) (3) (4) (5) (6) ");
        }
        System.out.println("-------------------------------");
        System.out.print("|  ");
        for (int i = board.length - 2; i >= board.length / 2; i--) {
            System.out.print("| ");
            System.out.printf("%-2s", board[i]);
        }
        System.out.print("|  |\n|");
        System.out.printf("%-2d|-----------------------|%2d|\n", Player.Two.getKalah(), Player.One.getKalah());
        System.out.print("|  ");
        for (int i = 0; i < (board.length / 2) - 1; i++) {
            System.out.print("| ");
            System.out.printf("%-2s", board[i]);
        }
        System.out.println("|  |");
        System.out.println("-------------------------------");
        System.out.println("    (1) (2) (3) (4) (5) (6) ");
    }

    // Returns true if the game is over (one side has no pieces)
    public boolean isOver() {
        return sum(Player.One) == 0 || sum(Player.Two) == 0;
    }

    // Checks if a player has won
    // Returns null if the game isn't over or the game is a tie
    public Player getWinner() {
        Player winner = null;
        if (isOver()) {
        

            Arrays.stream(Player.values()).forEach(player -> board[player.getKalahLoc()] += sum(player));

            int totalOne = Player.One.getKalah();
            int totalTwo = Player.Two.getKalah();
            if (totalOne > totalTwo) {
                winner = Player.One;
            } else if (totalOne < totalTwo) {
                winner = Player.Two;
            }
            for (int i = 0; i < board.length; i++) {
                if (i != Player.One.getKalahLoc() && i != Player.Two.getKalahLoc()) {
                    board[i] = 0;
                }
            }

        }
        return winner;
    }

    // Returns an integer of the number of pieces on Player m's side of the board
    public int sum(Player player) {
        int start = (player.getSkip() + 1) % board.length;

        return IntStream.range(start, start + (board.length - 1) / 2).sum();
    }

    // Accepts an integer position of the index of the board (0 based index)
    // Carries out a move for the player of the current turn
    // Returns true if the player gets to go again (landed in the kalah)
    public boolean markBoard(int pos) {
        int handAmount = board[pos];
        board[pos] = 0;
        while (handAmount > 0) {
            pos = (pos + 1) % board.length;
            handAmount--;
            if (pos == turn.getSkip()) {
                pos = (pos + 1) % board.length;
            }
            board[pos]++;
        }
        boolean taken = false;
        if (pos != turn.getKalahLoc() && board[pos] == 1 && board[getOpposite(pos)] != 0) {
            board[turn.getKalahLoc()] += board[pos] + board[getOpposite(pos)];
            board[pos] = 0;
            board[getOpposite(pos)] = 0;
            taken = true;
        }
        printBoard();
        if (taken) {
            System.out.println("Pieces taken!");
        } else if (!isOver() && pos == turn.getKalahLoc()) {
            System.out.println("Go again player " + turn + ". You landed in the Kalah.");
            return true;
        }
        return false;
    }

    // Returns index of the opposite position on the board
    private int getOpposite(int pos) {
        return board.length - 2 - pos;
    }

    // Reads a value from a scanner in the console
    public int readValue(Scanner scanner) {
        //Scanner scanner = new Scanner(System.in);
        int position = 2;
        boolean valid = false;
        while (!valid) {
            try {
                position = scanner.nextInt();
                if (position < 1 || position > 6) {
                    System.out.println("Invalid Position, input again:");
                } else {
                    if (turn == Player.One) {
                        position--;
                    } else {
                        position = board.length - 1 - position;
                    }
                    if (board[position] == 0) {
                        System.out.print("Spot is empty. Choose another spot:");
                    } else {
                        valid = true;
                    }
                }
            } catch (InputMismatchException inputMismatchException) {
                scanner.next(); // Print message for invalid input
                System.out.println("Invalid Position, input again:");
            }

        }
        return position;
    }
}