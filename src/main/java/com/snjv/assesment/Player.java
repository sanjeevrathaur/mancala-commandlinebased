package com.snjv.assesment;

import static com.snjv.assesment.Mancala.*;

public enum Player {
        One ((board.length - 2) / 2, board.length - 1),
        Two (board.length - 1, (board.length - 2) / 2);
        private final int kalahLocation;
        private final int kalahSkip;
        Player(int location, int skip) {
        	kalahLocation = location;
        	kalahSkip = skip;
        }
        int getKalah() {
        	return board[kalahLocation];
        }
        int getKalahLoc() {
        	return kalahLocation;
        }
        int getSkip() {
        	return kalahSkip;
        }

    }